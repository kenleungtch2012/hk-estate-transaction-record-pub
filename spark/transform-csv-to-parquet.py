from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.functions import concat
from pyspark.sql.functions import when
from pyspark.sql.functions import split
import datetime

spark = SparkSession.builder.appName("Transform CSV to Parquet").getOrCreate()

my_bucket_name = "notes.tecky.hk"
filename_to_read = "properties*.csv"
read_from_path = "s3a://{}/source_csv/estate-transaction-list-all-ready/{}".format(my_bucket_name,filename_to_read)

df = spark.read.csv(read_from_path)

# auto removed duplicated rows in all columns with same values.
newDF = df.distinct()

newDF = newDF.withColumnRenamed("_c0","registerDate")
newDF = newDF.withColumnRenamed("_c1","address")
newDF = newDF.withColumnRenamed("_c2","floor")
newDF = newDF.withColumnRenamed("_c3","flat")
newDF = newDF.withColumnRenamed("_c4","areaInFeet")
newDF = newDF.withColumnRenamed("_c5","dealInMillion")
newDF = newDF.withColumnRenamed("_c6","pricePerSqrFoot")
newDF = newDF.drop("_c7","_c8")

split_col = F.split(newDF['address'], '[\s\xa0]')

newDF = newDF.withColumn('address1', split_col.getItem(0))
newDF = newDF.withColumn('address2', split_col.getItem(1))
newDF = newDF.withColumn('address3', split_col.getItem(2))
newDF = newDF.withColumn('address4', split_col.getItem(3))
newDF = newDF.withColumn('address5', split_col.getItem(4))
newDF = newDF.withColumn('address6', split_col.getItem(5))
newDF = newDF.withColumn('address7',when(newDF.address3.isNull(), "").otherwise(newDF.address3))
newDF = newDF.withColumn('address8',when(newDF.address4.isNull(), "").otherwise(newDF.address4))
newDF = newDF.withColumn('address9',when(newDF.address5.isNull(), "").otherwise(newDF.address5))
newDF = newDF.withColumn('address10',when(newDF.address6.isNull(), "").otherwise(newDF.address6))
newDF = newDF.withColumn('address11',concat(newDF['address7'], newDF['address8'], newDF['address9']))

newDF = newDF.drop(newDF.address3)
newDF = newDF.drop(newDF.address4)
newDF = newDF.drop(newDF.address5)
newDF = newDF.drop(newDF.address6)

current_timestamp_string = str(datetime.datetime.now())
new_current_timestamp_string = ''.join(e for e in current_timestamp_string if e.isalnum())
new_path = "s3a://{}/transformed/temp/transformed_{}.parquet".format(my_bucket_name,new_current_timestamp_string)
newDF.write.parquet(new_path)