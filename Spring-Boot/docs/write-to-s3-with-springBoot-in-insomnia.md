1. ready your s3 cloud

2. ready Insomnia (Core version)

3. git pull the Spring Boot project to your local machine
    1. setup your `application.properties` with your s3 info. (DAE003 - writing to Amazon S3 with Spring boot)
    2. bootRun
    3. if have any error please try to refresh dependency, then bootRun again

4. after launch your Spring Boot server:
    1. insomnia: create request 
        -> POST: localhost:8080/transactionRecords
        copy this ONE demo JSON object ->
{
    "registerDate": "2018-12-31",
    "address": "沙田 沙田第一城 第05期 第49座",
    "floor": "5",
    "flat": "D",
    "areaInFeet": 303,
    "dealInMillion": 4.530,
    "pricePerSqrFoot": 14950,
    "detail": "詳情",
    "others": "最近放盤"
}

    2. browser:
        -> GET: localhost:8080/transactionRecords
    3. will write csv to S3 every 11 lines. (each file will have 11 lines of record)