package io.tecky.models;

public class TransactionRecord {

    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    private String registerDate;
    private String address;
    private String floor;
    private String flat;
    private Integer areaInFeet;
    private Float dealInMillion;
    private Integer pricePerSqrFoot;
    private String detail;
    private String others;

    public TransactionRecord() {
    }

    public TransactionRecord(
            String registerDate,
            String address,
            String floor,
            String flat,
            Integer areaInFeet,
            Float dealInMillion,
            Integer pricePerSqrFoot,
            String detail,
            String others
            ) { }

    public String getRegisterDate() {
        return registerDate;
    }

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getFlat() {
        return flat;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public Integer getAreaInFeet() {
        return areaInFeet;
    }

    public void setAreaInFeet(Integer areaInFeet) {
        this.areaInFeet = areaInFeet;
    }

    public Float getDealInMillion() {
        return dealInMillion;
    }

    public void setDealInMillion(Float dealInMillion) {
        this.dealInMillion = dealInMillion;
    }

    public Integer getPricePerSqrFoot() {
        return pricePerSqrFoot;
    }

    public void setPricePerSqrFoot(Integer pricePerSqrFoot) {
        this.pricePerSqrFoot = pricePerSqrFoot;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    @Override
    public String toString() {
        return "Data{" +
                "id=" + id +
                ", registerDate='" + registerDate + '\'' +
                ", address='" + address + '\'' +
                ", floor='" + floor + '\'' +
                ", flat='" + flat + '\'' +
                ", areaInFeet=" + areaInFeet +
                ", dealInMillion=" + dealInMillion +
                ", pricePerSqrFoot=" + pricePerSqrFoot +
                ", detail='" + detail + '\'' +
                ", others='" + others + '\'' +
                '}';
    }
}
