package io.tecky.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class HomeController {

    @RequestMapping(value = "/",method = RequestMethod.GET)
    public String SayHello(){
        return "Welcome to Housing!";
    }

}
