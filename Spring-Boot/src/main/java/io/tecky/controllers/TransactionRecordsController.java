package io.tecky.controllers;

import io.tecky.forms.TransactionRecordsForm;
import io.tecky.models.TransactionRecord;
import io.tecky.repositories.TransactionRecordsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransactionRecordsController {

    @Autowired
    TransactionRecordsRepository transactionRecordsRepository;

    @RequestMapping(value = "/transactionRecords",method = RequestMethod.GET)
    public List<TransactionRecord> getTransactionRecords(@RequestParam(value = "registerDate",required = false) String registerDate){
        if(registerDate != null){
            return transactionRecordsRepository.searchDataByRegisterDate(registerDate);
        }else{
            return transactionRecordsRepository.getTransactionRecords();
        }
    }

    @RequestMapping(value = "/transactionRecords",method = RequestMethod.POST)
    public final int addTransactionRecord(@RequestBody TransactionRecordsForm transactionRecordsForm){
        TransactionRecord transactionRecord = transactionRecordsForm.toData();
        return this.transactionRecordsRepository.addTransactionRecord(transactionRecord);
    }

}
