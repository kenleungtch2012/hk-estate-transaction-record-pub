package io.tecky.forms;
import io.tecky.models.TransactionRecord;

public class TransactionRecordsForm {

    private String registerDate;
    private String address;
    private String floor;
    private String flat;
    private Integer areaInFeet;
    private Float dealInMillion;
    private Integer pricePerSqrFoot;
    private String detail;
    private String others;

    public void setRegisterDate(String registerDate) {
        this.registerDate = registerDate;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public void setFlat(String flat) {
        this.flat = flat;
    }

    public void setAreaInFeet(Integer areaInFeet) {
        this.areaInFeet = areaInFeet;
    }

    public void setDealInMillion(Float dealInMillion) {
        this.dealInMillion = dealInMillion;
    }

    public void setPricePerSqrFoot(Integer pricePerSqrFoot) {
        this.pricePerSqrFoot = pricePerSqrFoot;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public TransactionRecord toData(){
        TransactionRecord data = new TransactionRecord();
        data.setAddress(this.address);
        data.setDetail(this.detail);
        data.setFlat(this.flat);
        data.setFloor(this.floor);
        data.setOthers(this.others);
        data.setRegisterDate(this.registerDate);
        data.setPricePerSqrFoot(this.pricePerSqrFoot);
        data.setDealInMillion(this.dealInMillion);
        data.setAreaInFeet(this.areaInFeet);
        return data;
    }
}
