import fs from 'fs';

const estateNameListFolder = './datasets/district.txt';
const targetFolder = './datasets/district-cleanup.txt';

async function readEstate(filePath: string) {
    const estateNameList = await fs.promises.readFile(filePath, { encoding: 'utf-8' })
    return estateNameList
}

async function writeEstate(filePath: string,data:string){
    await fs.promises.writeFile(filePath,data,"utf-8")
}

async function main() {
    const estateNameRawList = await readEstate(estateNameListFolder)
    const temp1 = estateNameRawList.replace(/\)/gm,"),")
    const temp2 = temp1.replace(/\,/gm, "\n")
    const temp3 = temp2.replace(/\、/gm, "\n")
    const temp4 = temp3.replace(/\([\s\S]*?\)/gm, "")
    const temp5 = temp4.replace(/[\s*\t*]\n[\s*\t*]/gm,"\n")
    const temp6 = temp5.replace(/\n(^[ \t]*\n)/gm,"\n")
    const temp7 = temp6.replace(/\n\s/gm,"\n")
    await writeEstate(targetFolder,temp7)
    const estateNameNewList = await readEstate(targetFolder)
    console.log(estateNameNewList)

}

main()
