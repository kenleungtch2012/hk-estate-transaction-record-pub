import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('deal_record',(table)=>{
        table.string('address8').nullable()
        table.string('address9').nullable()
        table.string('address10').nullable()
        table.string('address11').nullable()
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('deal_record',(table)=>{
        table.dropColumn('address8')
        table.dropColumn('address9')
        table.dropColumn('address10')
        table.dropColumn('address11')
    });
}

