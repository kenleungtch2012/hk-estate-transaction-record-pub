import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
      await knex.schema.alterTable('deal_record',(table)=>{
      table.date('registerDate').alter()
      table.integer('areaInFeet').alter()
      table.float('dealInMillion').alter()
      table.integer('pricePerSqrFoot').alter()

    });

}



export async function down(knex: Knex): Promise<void> {
       await knex.schema.alterTable('deal_record',(table)=>{
       table.string('registerDate').alter()
       table.string('areaInFeet').alter()
        table.string('dealInMillion').alter()
        table.string('pricePerSqrFoot').alter()
    });
}

