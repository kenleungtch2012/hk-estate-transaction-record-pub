import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    
  
    await knex.schema.createTable('deal_record',(table)=>{
        table.increments()
        table.string('registerDate').nullable()
        table.string('address').nullable()
        table.string('floor').nullable()
        table.string('flat').nullable()
        table.string('areaInFeet').nullable()
        table.string('dealInMillion').nullable()
        table.string('pricePerSqrFoot').nullable()

    });
    
   
}


export async function down(knex: Knex): Promise<void> {
   
    await knex.schema.dropTable('deal_record')
}

