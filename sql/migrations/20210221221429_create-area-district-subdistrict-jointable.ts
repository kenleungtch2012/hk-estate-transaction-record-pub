import * as Knex from "knex";

export async function up(knex: Knex): Promise<void> {

    await knex.schema.createTable('area',(table)=>{
        table.increments()
        table.string('name')
    })

    await knex.schema.renameTable('location','district')

    await knex.schema.createTable('sub_district',(table)=>{
        table.increments()
        table.string('name')
    })

    await knex.schema.createTable('area_district_subdistrict',(table)=>{
        table.increments()
        table.integer('sub_district_id')
        table.integer('district_id')
        table.integer('area_id')
    })
}

export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('district_subdistrict')
    await knex.schema.dropTable('sub_district')
    await knex.schema.renameTable('district','location')
    await knex.schema.dropTable('area')
}

