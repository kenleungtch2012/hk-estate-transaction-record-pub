import fs from "fs";
import * as Knex from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex("area_district_subdistrict").del();
    await knex("sub_district").del();
    await knex("district").del();
    await knex("area").del();

    // Inserts seed entries
    await fs.promises.readFile('./datasets/area.txt')
        .then(async (text) => {
            const areaArr = text.toString().split('\n')
            let areaObjectArr = []
            for (let word of areaArr) {
                areaObjectArr.push({ name: word })
            }
            await knex("area").insert(areaObjectArr).returning('id');
        })
        .catch(() => { console.log('failed to read area.txt') })

    await fs.promises.readFile('./datasets/district-cleanup.txt')
        .then(async (text) => {
            const districtArr = text.toString().split('\n')
            let districtObjectArr = []
            for (let word of districtArr) {
                districtObjectArr.push({ name: word })
            }
            await knex("district").insert(districtObjectArr).returning('id');
        })
        .catch(() => { console.log('failed to read district-cleanup.txt') })

    await fs.promises.readFile('./datasets/sub-district-cleanup.txt')
        .then(async (text) => {
            const subDistrictArr = text.toString().split('\n')
            let subDistrictObjectArr: Array<Object> = []
            let joinTableObjectArr: Array<Object> = []
            subDistrictArr.forEach((row, i) => {
                subDistrictObjectArr.push({ name: row.split(',')[0] })
                joinTableObjectArr.push({ sub_district_id: i + 1, district_id: parseInt(row.split(',')[1]), area_id: parseInt(row.split(',')[2]) })
            });
            await knex("sub_district").insert(subDistrictObjectArr).returning('id');
            await knex("area_district_subdistrict").insert(joinTableObjectArr).returning('id');
        })
        .catch(() => { console.log('failed to read sub-district-cleanup.txt') })



};
