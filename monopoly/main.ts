import express from 'express'
import {Request,Response} from 'express'

const app = express()

app.use(express.static('public'))

app.get('/',(req:Request,res:Response)=>{
res.send('/')
})

app.use((req,res)=>{
    res.sendStatus(404)
})

const PORT = 8080

app.listen(PORT,()=>{
    console.log(`Listening to http://localhost:${PORT}`)
})
