========== Company A ==========\
A) Typescript

1. Web scraping
    1. git pull as needed in the project root repository

    2. cd into folder /web-scrap

    3. run `yarn` to install necessary packages

    4. run `yarn start` to run the `main.ts` to start web scraping!
        ( It will loop to scrape with an estate name list like `<estate-name-list>.csv` in folder `datasets/`. )
    

2. Data producer
    1. git pull as needed in the project root repository

    2. cd into folder /web-scrap

    3. run `yarn` to install necessary packages

    4. Prepare your datasets! (if you are not going to completely run the web scraping application.)

    Create a new folder `/datasets/<estate-transaction-list>`then download all the *.csv from the link in `/datasets/source-link.txt` and put into this folder.

    !!! REMEMBER to change the file directory for reading by yourself.

    5. If you want to fetch the data source from an other machine which is in the same Wifi Network,
        please change the fetching URL into `http://<that-machine-ip>:8080/transactionRecords`. (eg. http://123.456.7.890:8080/transactionRecords)
        run the command below on terminal on that machine to check the ip: 
        - `ipconfig`: Windows
        - `ip addr`: WSL/Ubuntu
        - `ifconfig`: MacOS
        You can visit `http://<that-machine-ip>:8080/transactionRecords`
        in your local machine to check if access successfully.

    ( Need to run step.3 (Spring Boot Server) first! Before start to fetch.)

    6. run `yarn ts-node fetch.ts` to run the `fetch.ts` to start fetching data to Spring Boot API!

========== Company B ==========\
B) Java, Spring Boot & S3

3. Spring Boot Server
    1. In IntelliJ, Open the folder `/Spring-Boot` as project.

    2. On Gradle panel, right-click to refresh-dependencies.

    3. ready your s3 cloud bucket.

    4. setup your `application.properties` with your s3 info.
        `aws.bucket.name` can set to : `<my-bucket-name>/source_csv/from-spring-boot`
        you can create the folder `/source_csv` and `/from-spring-boot` in S3 by yourself.

    5. bootRun

    6. if have any error please try to refresh dependency, then bootRun again

    7. after bootRun succeeded, go back to 2) DAta producer -> step 6.

    8. On browser: `<that-machine-ip>:8080/transactionRecords`
        will see an array that will be reset when got 11 items,
        and the same time the Spring Boot application will write csv to S3 every 11 lines. (each file will have 11 lines of record)

    (9. using Insomnia instead of Data producer)\
        1. ready Insomnia (Core version)\
        2. after launch your Spring Boot server:\
            In insomnia: create request \
                -> POST: localhost:8080/transactionRecords\
        3. after send 11 requests, you can check if a file is uploaded to your S3 bucket.

----
C) Spark & S3

4. Spark ETL & S3
    1. setup your spark config with your S3 info to let your spark can access S3.

    2. follow instruction of `Spark Application`, and run the spark cluster.

    3. in our group project folder, `git pull` as needed in the project root repository.

    4. cd into folder `/spark`.

    5. activate your conda environment with `conda activate <myenv>`

    6. you can ready your S3 source data by uploading manually:

        i. If you are using ONLY shatin data:
            inside your s3 bucket '<my-bucket-name>/source_csv', create a folder 'csv-ready', 
            then upload the 112 csv files in this new folder. (be careful of the .DStore file of Mac)

        ii. If you are willing to use ALL estate data:
            inside your s3 bucket '<my-bucket-name>/source_csv', create a folder 'estate-transaction-list-all-ready', 
            then upload all the unzipped csv files in this new folder. (be careful of the .DStore file of Mac)

    6. in `transform-csv-to-parquet.py`, please change the path below to your own s3 path:
        my_bucket_name = "<my-bucket-root-name>"
        and also create a new folder named 'transformed' on your s3 bucket root directory.

    7. Please create a new folder `temp` in folder `transformed` in S3. `temp` is a temporary folder to separate from previous transformed files.
        Now this application will write transformed files into `temp`.

    7.  open another terminal,`cd` into the Spark application folder (you downloaded before) directory,
        in terminal, run `./bin/spark-submit <the-file-full-path>/transform-csv-to-parquet.py`

    8. then you can go back to your S3 bucket to check if files uploaded successfully.
        the folder name will ended with current time.

----
D) PostgreSQL Database\
5. Create Database, migration & seed

    1. in PostgreSQL, create a db named with `<db-name>`
    
    2. in our group project folder, git pull as needed in the project root repository

    3. `cd` into folder `/sql`

    4. run `yarn` to install necessary packages

    5. in folder `/sql`, create your own .env file with your db user info:
        DB_HOST=localhost
        DB_NAME=<db-name>
        DB_USERNAME=<my-psql-user-name>
        DB_PASSWORD=<my-psql-user-pw>

    6. in terminal, run `yarn knex migrate:latest`
        then go back to `psql`, check if the table `deal_record` is created.

    7. in terminal. run `yarn knex seed:run`
        then go back to `psql`, check if a table `location` is created with seed data.

----
E) Spark, S3 & PostgreSQL\
6. Spark read from S3, load to PostgreSQL

    1. follow the instruction for `Spark Application`, and run the spark cluster.

    2. in our group project folder, `git pull` as needed in the project root repository

    3. `cd` into folder `/spark`

    4. activate your conda environment with `conda activate <myenv>`

    5. in terminal, run `pip install findspark` to install `findspark`.

    5. in `load-data-into-psql.py`, please change the path below to your own info:
    ```
        findspark.init('<my-downloaded-spark-application-folder-full-root-path>')

        db_config = {
            "url":'jdbc:postgresql://localhost:5432/<db-name>',
            "dbtable":'<my-db-name>',
            "user":'<my-psql-user-name>',
            "password":<my-psql-user-pw>,
            "driver" :'org.postgresql.Driver'
        }
    ```

        here is to read the file within a folder named `filename-prefix*.parquet`, make sure the folder is exist.
        bucketname = `my-s3-bucket-root-name`

        REMARK: This application will read files from 's3://<my-bucket-name>/transformed/temp/'

    7.  open another terminal, `cd` into the Spark application folder (you downloaded before) directory,
        in terminal, run `./bin/spark-submit <the-file-full-path>/transform-csv-to-parquet.py`

    8. in psql shell, run `select * from deal_record` to check content 
        OR run `select count(*) from deal_record` to check rows' count.

----
F) PostgreSQL & BI

i) Power BI (in Windows)